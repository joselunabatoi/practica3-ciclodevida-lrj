package dam.jose.p0_helloworld;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends AppCompatActivity {
    private static final String DEBUG_TAG = "LOG-";


    private void notify(String eventName){
        String activityname = this.getClass().getSimpleName();
        String CHANNEL_ID = "My_LifeCycle";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR_0_1){

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,"My_LifeCycle", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Lifecycle event");
            notificationChannel.setShowBadge(true);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null){
                notificationManager.createNotificationChannel(notificationChannel);
            }

        }
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName+" "+activityname)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);
        notificationManagerCompat.notify((int) System.currentTimeMillis(),notificationBuilder.build());


    }




    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG,"onStart");
        notify("onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG,"onStop");
        notify("onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG,"onPause");
        notify("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG,"onResume");
        notify("onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG,"onRestart");
        notify("onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()){
            Log.i(DEBUG_TAG,"onDestroy-Usuario");
            notify("onDestroy-Usuario");
        }else{
            Log.i(DEBUG_TAG,"onDestroy-Sistema");
            notify("onDestroy-Sistema");
        }
    }

    @Override
    public boolean isFinishing() {
        return super.isFinishing();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(DEBUG_TAG,"onSaveInstanceState");
        notify("onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(DEBUG_TAG,"onRestoreInstanceState");
        notify("onRestoreInstanceState");
    }

}
