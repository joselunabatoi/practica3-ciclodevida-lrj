package dam.jose.p0_helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class NextActivity extends LogActivity {
    private static final String DEBUG_TAG = "LogsAndroid-2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        setupUI();
    }

    private void setupUI() {
        Button btNextActivity = findViewById(R.id.back);

        btNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(NextActivity.this,MainActivity.class));
            }
        });
    }
    }